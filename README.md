# POC - expression language

## Overview

This POC has a React application to handle expression language of Symfony.

It also has a simple API (Symfony) to fetch/store the expression language.

## Install

Just run:

```
docker-compose up
```

After the application is running, you can access: http://localhost:3000/

The API is available at http://localhost:3001/exp-language (GET/POST)

