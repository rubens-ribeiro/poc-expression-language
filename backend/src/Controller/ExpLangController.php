<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ExpLangController extends AbstractController
{
    /**
     * @Route("/exp-language", methods={"GET","HEAD"})
     */
    public function fetch(): Response
    {
        $exp = $this->fetchExpression();

        return $this->json($exp, Response::HTTP_OK);
    }

    /**
     * @Route("/exp-language", methods={"POST"})
     */
    public function save(Request $request): Response
    {
        // Validate Request
        try {
            $reqJson = json_decode($request->getContent(), true);
            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new \RuntimeException('JSON error: ' . json_last_error_msg());
            }
            if (empty($reqJson['exp']) || empty($reqJson['names'])) {
                throw new \RuntimeException('Invalid request body');
            }

            $expStr = $this->compile($reqJson['exp']);

            $expressionLanguage = new ExpressionLanguage();
            $expressionLanguage->lint($expStr, $reqJson['names']);
        } catch (\Exception $e) {
            return $this->json(['message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        // Save
        try {
            $this->saveExpression($reqJson);
        } catch (\Exception $e) {
            return $this->json(['message' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $this->json(['exp' => $expStr], Response::HTTP_OK);
    }

    private function compile(array $exp): string
    {
        return ($exp['type'] === 'literal')
        ? '(' . $exp['val'] . ')'
        : '(' . implode(sprintf(' %s ', $exp['type']), array_map([$this, 'compile'], $exp['exps'])) . ')';
    }

    private function fetchExpression()
    {
        if (!is_file('/tmp/exp.json')) {
            return [
                'names' => ['teste'],
                'exp'   => ['type' => 'literal', 'val' => 'teste == 1'],
            ];
        }

        $content = file_get_contents('/tmp/exp.json');
        return json_decode($content, true);
    }

    private function saveExpression(array $exp)
    {
        if (!file_put_contents('/tmp/exp.json', json_encode($exp))) {
            throw new \RuntimeException('Failed to save');
        }
    }
}
