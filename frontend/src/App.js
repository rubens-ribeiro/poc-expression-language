import React, { useState, useEffect } from 'react';
import './App.css';

/**
 * Component: Expression Language Element
 *
 * @component
 * @param {object} props
 * @param {object} props.expression - The element to show
 * @param {function} props.setExpressions - The function to update the list of expressions
 * @return React Element
 */
function ExpressionLanguageElement(props) {
  const { expression, setExpressions } = props;

  const expCheckbox = <input type="checkbox" checked={expression.checked || false} onChange={e => { expression.checked = e.target.checked; setExpressions(exps => [...exps]); }} />;

  let content;
  switch (expression.type) {
    case 'or':
    case 'and':
      content = (
        <>
          <div className="exp-content">
            <label>
              {expCheckbox}
              <span className="exp-group">{expression.type.toUpperCase()}</span>
            </label>
          </div>
          <div className="exp-content">
            {expression.exps.map(subExpression =>
              <ExpressionLanguageElement key={subExpression.id} setExpressions={setExpressions} expression={subExpression} />
            )}
          </div>
        </>
      );
      break;
    case 'literal':
    default:
      content = (
        <div className="exp-content">
          {expCheckbox}
          <input className="exp-literal" type="text" value={expression.val} onChange={e => { expression.val = e.target.value; setExpressions(exps => [...exps]); }} />
        </div>
      );
      break;
  }
  return (
    <div className="exp-area">
      <div className="exp">
        <div className="exp-open" />
        {content}
        <div className="exp-close" />
      </div>
    </div>
  );
}

/**
 * Component: Expression Language Form
 *
 * States:
 * {string} names - List of names used by expression
 * {object[]} expressions - List of expressions
 * {string} newExpression - Text of the new expression
 * {bool} debug - Debug
 *
 * @component
 * @param {object}   props
 * @param {string}   props.names - Default list of names used by expression
 * @param {object[]} props.expressions - Default list of expressions
 * @param {string} props.newExpression - Default text for new expression
 * @param {bool} props.debug - Default debug state
 * @return React Element
 */
function ExpressionLanguageForm(props) {
  const [names, setNames] = useState(props.names || '');
  const [expressions, setExpressions] = useState(props.expressions || []);
  const [newExpression, setNewExpression] = useState(props.newExpression || '');
  const [debug, setDebug] = useState(props.debug || false);

  const hasGroups  = hasExpressionsGroups(expressions);
  const hasChecked = hasExpressionsChecked(expressions);

  return (
    <form className="exp-builder" onSubmit={(e) => { e.preventDefault(); save(names, expressions); }}>
      <fieldset className="area-builder">
        <legend>Expression Builder</legend>
        <div className="new-expression">
          <label>
            New Expression:
            <input
              type="text"
              value={newExpression}
              placeholder="expression"
              autoComplete="off"
              onChange={e => setNewExpression(e.target.value)}
              onKeyDown={e => { if (e.key == 'Enter') { e.preventDefault(); setExpressions(addExpression(newExpression, expressions)); }; }}
            />
          </label>
          <button type="button" disabled={newExpression.length === 0} onClick={() => setExpressions(addExpression(newExpression, expressions))}>Add</button>
        </div>
        <div className="expressions">
          {expressions.map(exp =>
            <ExpressionLanguageElement key={exp.id} expression={exp} setExpressions={setExpressions} />)
          }
        </div>
        <div className="operations">
          <button type="button" disabled={!hasGroups} onClick={() => setExpressions(ungroupAll(expressions))}>Ungroup All</button>
          <button type="button" disabled={!hasChecked} onClick={() => setExpressions(ungroup(expressions))}>Ungroup</button>
          <button type="button" disabled={!hasChecked} onClick={() => setExpressions(remove(expressions))}>Delete</button>
          <button type="button" disabled={!hasChecked} onClick={() => setExpressions(group(expressions, 'or'))}>OR</button>
          <button type="button" disabled={!hasChecked} onClick={() => setExpressions(group(expressions, 'and'))}>AND</button>
        </div>
      </fieldset>

      <fieldset className="area-names">
        <legend>Names</legend>
        <p>Names: <input type="text" name="names" value={names} autoComplete="off" onChange={e => setNames(e.target.value)} /></p>
        <p><small>List of names used by the expression (separated by comma).</small></p>
      </fieldset>

      <fieldset className="area-preview">
        <legend>Preview</legend>
        <pre>{expressions.length === 1 ? buildPreview(expressions[0]) : 'Must group the expressions first' }</pre>
      </fieldset>

      <div className="operations">
        <button type="submit" disabled={expressions.length !== 1}>Save!</button>
      </div>

      <div className="debug-option">
        <label>
          <input type="checkbox" checked={debug} onChange={e => setDebug(e.target.checked)} />
          Debug
        </label>
      </div>

      {debug &&
        <fieldset className="area-debug">
          <legend>Debug</legend>
          <pre>New expression: {JSON.stringify(newExpression, null, 2)}</pre>
          <pre>Names: {JSON.stringify(names.split(',').filter(e => e), null, 2)}</pre>
          <pre>Expressions: {JSON.stringify(expressions, null, 2)}</pre>
        </fieldset>
      }
    </form>
  );
}

/**
 * Component: App
 *
 * States:
 * {null|object} error
 * {bool} isLoaded - Whether the data was fetched
 * {string|null} names - Names used by the expression
 * {object[]|null} expressions - List of expressions
 *
 * @component
 * @return React Element
 */
function App() {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [names, setNames] = useState('');
  const [expressions, setExpressions] = useState([]);

  useEffect(() => {
    if (isLoaded) {
      return;
    }
    fetch('http://localhost:3001/exp-language')
      .then(res => res.json())
      .then(
        (json) => {
          setNames(json.names.join(','));
          setExpressions(json.exp ? [json.exp] : []);
          setIsLoaded(true);
        },
        (err) => {
          setIsLoaded(true);
          setError(err);
        },
      );
  });

  if (error) {
    return <p>Error: {error.message}</p>
  }

  if (!isLoaded) {
    return <p>Loading...</p>;
  }

  return (
    <div className="app">
      <ExpressionLanguageForm
        names={names}
        expressions={initExpressions(expressions)}
      />
    </div>
  );
}


/// Helper functions


/**
 * Global array with generated IDs
 * to avoid duplicating random IDs
 */
const generatedIds = [];

/**
 * Build a random ID with a prefix
 * @param {string} prefix
 * @return {string}
 */
function buildRandomId(prefix) {
  let id = '';

  do {
    id = `${prefix}${parseInt(Math.random() * 100000, 10)}`;
  } while (generatedIds.includes(id));
  generatedIds.push(id);

  return id;
}

/**
 * Initialize the expressions adding a random ID and marking it as not checked
 * @param {object[]} expressions
 * @return {object[]}
 */
function initExpressions(expressions) {
  return expressions.map(exp => {
    exp.id = buildRandomId('exp-no-');
    exp.checked = false;
    if (exp.type !== 'literal') {
      exp.exps = initExpressions(exp.exps);
    }
    return exp;
  })
}

/**
 * Return whether there is at least one expression checked (recursivelly)
 * @param {object[]} expressions
 * @return {bool}
 */
function hasExpressionsChecked(expressions) {
  return expressions.some(exp => exp.checked)
    || expressions.filter(exp => exp.type !== 'literal').some(exp => hasExpressionsChecked(exp.exps));
}

/**
 * Return whether there is at least one expression group ("and" or "or")
 * @param {object[]} expressions
 * @return {bool}
 */
function hasExpressionsGroups(expressions) {
  return expressions.some(exp => exp.type !== 'literal');
}

/**
 * Build the preview of an expression as a string
 * @paran {object} expression
 * @return string
 */
function buildPreview(expression) {
  const content = expression.type == 'literal'
    ? expression.val
    : expression.exps.map(buildPreview).join(` ${expression.type} `);

  return `(${content})`;
}

/**
 * Ungroup all expressions recursivelly
 * @param {object[]} expressions
 * @return {object[]}
 */
function ungroupAll(expressions) {
  const result = [];
  expressions.forEach(exp => {
    if (exp.type === 'literal') {
      result.push(exp);
    } else {
      result.push(...ungroupAll(exp.exps));
    }
  });

  return result;
}

/**
 * Ungroup the expressions that are checked
 * (it unchecks all expressions after the operation)
 * @param {object[]} expressions
 * @return {object[]}
 */
function ungroup(expressions) {
  const result = [];
  expressions.forEach(exp => {
    if (exp.checked && exp.type !== 'literal') {
      result.push(...exp.exps);
    } else {
      result.push(exp);
    }
  });

  return result.map(exp => {
    if (exp.type !== 'literal') {
      return {
        ...exp,
        exps: ungroup(exp.exps),
        checked: false,
      };
    }
    return {...exp, checked: false};
  });

  return result;
}

/**
 * Add a new expression to the list of expressions
 * @param {string} expressionText - Literal expression text
 * @param {object[]} expressions - List of expressions
 * @return {object[]}
 */
function addExpression(expressionText, expressions) {
  const newExp = {
    type: 'literal',
    val: expressionText,
    id: buildRandomId('exp-no-'),
    checked: false,
  };

  return [...expressions, newExp];
}

/**
 * Remove the expressions that are checked
 * (it unchecks all expressions after the operation)
 * @param {object[]} expressions
 * @return {object[]}
 */
function remove(expressions) {
  return expressions
    .filter(exp => !exp.checked)
    .map(exp => {
      if (exp.type !== 'literal') {
        return {
          ...exp,
          exps: remove(exp.exps),
        };
      }
      return exp;
    });
}

/**
 * Replace the checked expressions of the same level into a new
 * expression of type "AND" or "OR".
 * (it unchecks all expressions after the operation)
 * @param {object[]} expressions
 * @param {string} groupType - "and" or "or"
 * @return {object[]}
 */
function group(expressions, groupType) {
  const result = [];

  const indexExpChecked = expressions.findIndex(exp => exp.checked);
  if (indexExpChecked >= 0) {
    const expsChecked = expressions
      .filter(exp => exp.checked)
      .map(exp => ({ ...exp, checked: false }));

    const expGroup = {
      type: groupType,
      exps: expsChecked,
      id: buildRandomId('exp-no-'),
      checked: false,
    };

    const expsUnchecked = expressions
      .filter(exp => !exp.checked);

    result.push(...expsUnchecked);
    result.splice(indexExpChecked, 0, expGroup);
  } else {
    result.push(...expressions);
  }

  return result.map(exp => {
    if (exp.type !== 'literal') {
      return {
        ...exp,
        exps: group(exp.exps, groupType),
      };
    }
    return exp;
  });
}

/**
 * Extract only the relevant data from an expression (removes "id" and "checked")
 * @param {object} expression
 * @return {object}
 */
function filterExpression(expression) {
  return {
    type: expression.type,
    val: expression.val,
    exps: expression.exps ? expression.exps.map(filterExpression) : undefined,
  };
}

/**
 * Save the expression
 * @param {string} names
 * @param {object[]} expressions
 * @return Promise
 */
function save(names, expressions) {
  if (expressions.length !== 1) {
    console.error('Expressions with invalid length');
    return;
  }
  const filteredExpression = filterExpression(expressions[0]);

  console.log('SENDING: ', JSON.stringify(filteredExpression, null, 2));

  fetch(
    'http://localhost:3001/exp-language',
    {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        names: names.split(/\s*,\s*/),
        exp: filteredExpression,
      })
    },
  )
    .then(res => res.json())
    .then(
      (json) => {
        if (json.exp) {
          window.alert(`Saved: ${json.exp}`);
        } else {
          window.alert(`Error: ${json.message}`);
        }
      },
      (err) => {
        console.error(err);
      },
    );
}

export default App;
